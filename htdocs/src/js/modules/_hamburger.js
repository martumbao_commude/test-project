export const hamburger = () => {

    $('.js-hamburger').click(function () {
        $(this).toggleClass('is-active');
        $('.siteHeader').toggleClass('is-active');
        $('.hamburger__menu').slideToggle();
    })
}