</main><!-- siteContent -->
<!-- //////////////////// CONTENT END //////////////////// -->




<!-- //////////////////// FOOTER START //////////////////// -->
<footer class="siteFooter">
    <div class="siteFooter__innerArea">
        <div class="siteFooter__clmArea">
            <div class="siteFooter__leftArea">
                <h2 class="siteFooter__logo">
                    <svg id="logo" class="siteFooter__logoImg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 380.115 73.416">
                        <path id="パス_20" data-name="パス 20" d="M211.607,272.871V199.455h9.633v63.662h24.755v9.754Z" transform="translate(-211.607 -199.455)"/>
                        <path id="パス_21" data-name="パス 21" d="M234.129,272.871a6.915,6.915,0,0,1-5.075-2.2,7.455,7.455,0,0,1-2.175-5.454v-58.1a7.455,7.455,0,0,1,2.175-5.454,6.915,6.915,0,0,1,5.075-2.2h23.512a7.223,7.223,0,0,1,5.438,2.2,7.552,7.552,0,0,1,2.123,5.454v58.1a7.552,7.552,0,0,1-2.123,5.454,7.223,7.223,0,0,1-5.438,2.2Zm5.9-9.649h11.7a3.734,3.734,0,0,0,2.848-1.154,3.973,3.973,0,0,0,1.087-2.832V213.089a3.927,3.927,0,0,0-1.087-2.884,3.826,3.826,0,0,0-2.848-1.1h-11.7a3.457,3.457,0,0,0-2.538,1.1,3.927,3.927,0,0,0-1.088,2.884v46.147a3.973,3.973,0,0,0,1.088,2.832A3.385,3.385,0,0,0,240.033,263.222Z" transform="translate(-182.939 -199.455)"/>
                        <path id="パス_22" data-name="パス 22" d="M252.121,272.871a7.158,7.158,0,0,1-5.285-2.2,7.337,7.337,0,0,1-2.172-5.349V206.9a7.468,7.468,0,0,1,7.458-7.446h23.512a7.473,7.473,0,0,1,7.458,7.446v16.361l-9.633-2.2V212.88a3.667,3.667,0,0,0-1.09-2.674,3.563,3.563,0,0,0-2.638-1.1h-11.7a3.573,3.573,0,0,0-2.641,1.1,3.664,3.664,0,0,0-1.088,2.674v46.462a3.664,3.664,0,0,0,1.088,2.674,3.573,3.573,0,0,0,2.641,1.1h11.7a3.563,3.563,0,0,0,2.638-1.1,3.667,3.667,0,0,0,1.09-2.674V249.168h-9.529l2.175-9.754H283.09v25.905a7.329,7.329,0,0,1-2.175,5.349,7.152,7.152,0,0,1-5.282,2.2Z" transform="translate(-149.556 -199.455)"/>
                        <path id="パス_23" data-name="パス 23" d="M269.733,272.871a6.919,6.919,0,0,1-5.075-2.2,7.455,7.455,0,0,1-2.175-5.454v-58.1a7.456,7.456,0,0,1,2.175-5.454,6.919,6.919,0,0,1,5.075-2.2h23.512a7.223,7.223,0,0,1,5.438,2.2,7.552,7.552,0,0,1,2.123,5.454v58.1a7.552,7.552,0,0,1-2.123,5.454,7.223,7.223,0,0,1-5.438,2.2Zm5.907-9.649h11.7a3.732,3.732,0,0,0,2.846-1.154,3.975,3.975,0,0,0,1.09-2.832V213.089a3.929,3.929,0,0,0-1.09-2.884,3.824,3.824,0,0,0-2.846-1.1h-11.7a3.459,3.459,0,0,0-2.541,1.1,3.928,3.928,0,0,0-1.087,2.884v46.147a3.973,3.973,0,0,0,1.087,2.832A3.387,3.387,0,0,0,275.639,263.222Z" transform="translate(-116.107 -199.455)"/>
                        <path id="パス_24" data-name="パス 24" d="M294.519,272.871V209.209h-14.4v-9.754h38.427v9.754h-14.4v63.662Z" transform="translate(-82.994 -199.455)"/>
                        <path id="パス_25" data-name="パス 25" d="M310.988,272.871V237.212L296.8,199.455h9.633l9.426,25.066,9.322-25.066h9.636l-14.193,37.757v35.659Z" transform="translate(-51.69 -199.455)"/>
                        <path id="パス_26" data-name="パス 26" d="M313.475,272.871V199.455h31.07A7.563,7.563,0,0,1,352,207.006v22.549a7.563,7.563,0,0,1-7.458,7.551H323.1v35.764Zm9.63-45.518h15.539a3.578,3.578,0,0,0,2.641-1.1,3.664,3.664,0,0,0,1.088-2.674v-10.7a3.738,3.738,0,0,0-3.729-3.671H323.1Z" transform="translate(-20.385 -199.455)"/>
                        <path id="パス_27" data-name="パス 27" d="M330.33,272.871V199.455h38.531V209.1h-28.9v18.249h19.265l-2.178,9.754H339.963v26.01h28.9v9.754Z" transform="translate(11.254 -199.455)"/>
                    </svg>
                </h2>
                <address class="siteFooter__address">
                    住所が入ります。住所が入ります。住所が入ります。<br>
                    電話番号がはいります。電話番号がはいります。
                </address>
            </div>

            <div class="siteFooter__rightArea">
                <nav class="siteFooter__nav">
                    <ul class="siteFooter__pageList">
                        <li class="siteFooter__pageItem">
                            <a href="#" class="siteFooter__pageLink">ページ名</a>
                        </li>
                        <li class="siteFooter__pageItem">
                            <a href="#" class="siteFooter__pageLink">ページ名</a>
                        </li>
                    </ul>
                    <ul class="siteFooter__pageList">
                        <li class="siteFooter__pageItem">
                            <a href="#" class="siteFooter__pageLink">ページ名</a>
                        </li>
                        <li class="siteFooter__pageItem">
                            <a href="#" class="siteFooter__pageLink">ページ名</a>
                        </li>
                    </ul>
                    <ul class="siteFooter__pageList">
                        <li class="siteFooter__pageItem">
                            <a href="#" class="siteFooter__pageLink">ページ名</a>
                            <ul class="siteFooter__childPageList">
                                <li class="siteFooter__childPageItem">
                                    <a href="#" class="siteFooter__childPageLink">子ページ名</a>
                                </li>
                                <li class="siteFooter__childPageItem">
                                    <a href="#" class="siteFooter__childPageLink">子ページ名</a>
                                </li>
                            </ul>
                        </li>
                        <li class="siteFooter__pageItem">
                            <a href="#" class="siteFooter__pageLink">ページ名</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <p class="siteFooter__copy">©2020 株式会社○○ All Rights Reserved.</p>
    </div>
</footer>
<!-- //////////////////// FOOTER END //////////////////// -->



<?php wp_footer(); ?>
</body>
</html>
